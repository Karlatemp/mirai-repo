# Mirai Repo

用于放置包版本信息。

## 如何在此仓库被列出

1. 包必须遵循`AGPLv3`协议开源
1. `JAR`必须已在`Maven Central`或`JCenter`上线
1. 提交`Pull Request`
